package part1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class PhoneBook {

	public void read() throws IOException {
		String file = "phonebook.txt";
		FileReader reader = null;
		reader = new FileReader(file);
		BufferedReader b = new BufferedReader(reader);
		String line;
		System.out.print("----------Phone Book-----------\n");
		System.out.print("Name,Phone\n\n");
		for (line = b.readLine(); line != null; line = b.readLine()) {
			System.out.println(line);

		}
	}

	public void addNumber() throws IOException {
		FileWriter file = new FileWriter("phonebook.txt", true);
		InputStreamReader inReader = new InputStreamReader(System.in);
		BufferedReader buffer = new BufferedReader(inReader); 
		PrintWriter x = new PrintWriter(new BufferedWriter(file));
		System.out.println("Input new contact : ");
		String line = buffer.readLine();
		while (!line.equals("bye")) {;
			x.println(line);
			line = buffer.readLine();
		}
		
		x.flush();

	}
}
