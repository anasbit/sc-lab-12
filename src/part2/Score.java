package part2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Score {

	public void read() throws IOException {
		String line;
		double score = 0.0;
		double sum = 0.0;

		String file = "homework.txt";
		FileReader reader = new FileReader(file);
		BufferedReader b = new BufferedReader(reader);

		System.out.print("----------HomeWork Score-----------\n");
		System.out.print("Name, Average\n");
		System.out.println("==================");

		FileWriter write = new FileWriter("average.txt");
		PrintWriter x = new PrintWriter(new BufferedWriter(write));

		for (line = b.readLine(); line != null; line = b.readLine()) {
			String[] list = line.split(",");

			for (int i = 1; i < list.length; i++) {
				score = Double.parseDouble(list[i]);
				sum += score;
			}

			String name = list[0];
			x.println(name + "\t" + sum / (list.length - 1));
			x.flush();
			System.out.println(name + "\t" + sum / (list.length - 1));
			sum = 0.0;
		}

	}
}
